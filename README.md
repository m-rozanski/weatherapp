WeatherApp Demo
====================

WeatherApp Demo is my university project application, which allows you to check current weather for many cities at once.
List of cities you want to follow is saved in local database together with current weather data for each one, so that you don't have to search for cities every time, and you still have data in case you lose your Internet connection.

I've decided to use [OpenWeatherMap][weather] for weather source.

---

Technologies used in this project:
====================

* MVP
* [Retrofit][retrofit]
* [RxJava 2][rxjava]
* [Dagger 2][dagger]
* [Room][room]
* [Mockito][mockito]
* [Stetho][stetho]
* [Picasso][picasso]
* [Gson][gson]
* [RecyclerView][recycler]
* [CardView][card]


[weather]: https://openweathermap.org/
[retrofit]: https://square.github.io/retrofit/
[rxjava]: https://github.com/ReactiveX/RxJava
[dagger]: https://github.com/google/dagger
[room]: https://developer.android.com/jetpack/androidx/releases/room
[mockito]: https://site.mockito.org/
[stetho]: http://facebook.github.io/stetho/
[picasso]: https://square.github.io/picasso/
[gson]: https://github.com/google/gson
[recycler]: https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView
[card]: https://developer.android.com/reference/kotlin/androidx/cardview/widget/CardView