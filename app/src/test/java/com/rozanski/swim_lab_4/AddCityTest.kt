package com.rozanski.swim_lab_4

import com.nhaarman.mockito_kotlin.verify
import com.rozanski.swim_lab_4.ui.ShowWeatherContract
import com.rozanski.swim_lab_4.ui.ShowWeatherPresenter
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.MockitoAnnotations

class AddCityTest {
    @Mock
    private lateinit var view: ShowWeatherContract.View
    private lateinit var presenter: ShowWeatherPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = ShowWeatherPresenter()
        presenter.attachView(view)
    }

    @Test
    fun addCity_blankCityName_shouldShowMessage() {
        val cityName = " "
        presenter.addCity(cityName)
        verify(view).showMessage(R.string.invalidCityName)
    }

    @Test
    fun addCity_blankCityName_shouldNotShowMessage() {
        val cityName = "cityName"
        presenter.addCity(cityName)
        verify(view, never()).showMessage(R.string.invalidCityName)
    }
}