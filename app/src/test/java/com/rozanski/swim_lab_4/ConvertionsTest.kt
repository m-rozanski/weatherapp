package com.rozanski.swim_lab_4

import com.google.gson.Gson
import com.rozanski.swim_lab_4.database.CurrentWeatherEntity
import com.rozanski.swim_lab_4.model.currentWeatherResponse.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ConvertionsTest {
    private lateinit var currentWeatherResponse: CurrentWeatherResponse
    private lateinit var currentWeatherEntity: CurrentWeatherEntity

    @Before
    fun setup() {
        val clouds = Clouds(5)
        val coord = Coordinates(56.7, 30.8)
        val main = WeatherValues(2.0, 300.0, 40.3, 40.5, 39.8)
        val sys = Sys("Country", 51651, 65.9, 7, 8, 9)
        val weather = WeatherConditionDetails("Description", "icon", 65, "main")
        val wind = Wind(65.95, 65.75)

        currentWeatherResponse =
            CurrentWeatherResponse(
                "base", clouds, 75, coord, 58,
                15164, main, "name", sys, 1, listOf(weather), wind
            )

        val gson = Gson()
        val str: String = gson.toJson(currentWeatherResponse)
        currentWeatherEntity = CurrentWeatherEntity(currentWeatherResponse.id, str)
    }

    @Test
    fun convert_currentWeatherResponse_to_currentWeatherEntity() {
        val entity = convertToEntity(currentWeatherResponse)
        assertEquals(currentWeatherEntity, entity)
    }

    @Test
    fun convert_currentWeatherEntity_to_currentWeatherResponse() {
        val response = convertToResponse(currentWeatherEntity)
        assertEquals(currentWeatherResponse, response)
    }

}