package com.rozanski.swim_lab_4

import com.rozanski.swim_lab_4.ui.ShowWeatherContract
import com.rozanski.swim_lab_4.ui.ShowWeatherPresenter
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class ShowingMessagesTest {
    @Mock
    private lateinit var view: ShowWeatherContract.View
    private lateinit var presenter: ShowWeatherPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = ShowWeatherPresenter()
        presenter.attachView(view)
    }

    @Test
    fun showMessage_viewUpdatedfForMessage() {
        val msg = R.string.refreshing
        presenter.showMessage(msg)
        verify(view).showMessage(msg)
    }
}