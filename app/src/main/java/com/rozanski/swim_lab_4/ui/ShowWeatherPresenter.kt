package com.rozanski.swim_lab_4.ui

import android.util.Log
import com.rozanski.swim_lab_4.R
import com.rozanski.swim_lab_4.convertToEntity
import com.rozanski.swim_lab_4.convertToResponse
import com.rozanski.swim_lab_4.database.CurrentWeatherEntity
import com.rozanski.swim_lab_4.database.CurrentWeatherRepository
import com.rozanski.swim_lab_4.di.component.DaggerShowWeatherPresenterComponent
import com.rozanski.swim_lab_4.di.module.ShowWeatherModelModule
import com.rozanski.swim_lab_4.model.ShowWeatherModel
import com.rozanski.swim_lab_4.model.currentWeatherResponse.CurrentWeatherResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class ShowWeatherPresenter : ShowWeatherContract.Presenter {
    private var view: ShowWeatherContract.View? = null
    @Inject
    lateinit var model: ShowWeatherModel
    private val db = CurrentWeatherRepository()
    private val disposable = CompositeDisposable()
    private val CODE_ADD = 0
    private val CODE_REFRESH = 1
    private lateinit var currentWeatherList: List<CurrentWeatherResponse>

    private var itemsToRefresh = 0
    private var actualListToRefresh = mutableListOf<CurrentWeatherEntity>()

    init {
        DaggerShowWeatherPresenterComponent.builder().showWeatherModelModule(ShowWeatherModelModule()).build()
            .inject(this)
    }


    fun setup() {
        db.setup()
    }


    override fun loadData() {
        disposable.add(
            db.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    currentWeatherList = it.map { convertToResponse(it) }
                    view?.updateData(currentWeatherList)
                }, {
                    showMessage(R.string.loadingDatabaseError)
                    Log.d("My", it.message)
                })
        )
    }


    override fun addCity(cityName: String) {
        if (cityName.isBlank())
            view?.showMessage(R.string.invalidCityName)
        else
            model.getCurrentWeather(this, cityName, CODE_ADD)
    }


    override fun refresh() {
        view?.showMessage(R.string.refreshing)
        val presenter = this
        doAsync {
            val cities = currentWeatherList
            itemsToRefresh = currentWeatherList.size

            cities.forEach {
                model.getCurrentWeather(presenter, it.name, CODE_REFRESH)
            }
        }
    }


    override fun delete(item: CurrentWeatherResponse) {
        disposable.add(db.delete(convertToEntity(item))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("My", "Delete: ${item.name}")
            }, {
                showMessage(R.string.notExpectedBehaviour)
                Log.d("My", it.message)
            }
            ))
    }


    fun onApiResponse(item: CurrentWeatherResponse?, code: Int) {
        if (item != null) {
            when (code) {
                CODE_ADD -> insertToDatabase(item)

                CODE_REFRESH -> {
                    actualListToRefresh.add(convertToEntity(item))
                    if (actualListToRefresh.size == itemsToRefresh) {
                        val list = actualListToRefresh.toList()
                        insertToDatabase(list)
                        actualListToRefresh.clear()
                    }
                }
            }
        } else view?.showMessage(R.string.addingCityError)
    }


    private fun insertToDatabase(item: CurrentWeatherResponse) {
        disposable.add(db.insert(convertToEntity(item))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("My", "Add: ${item.name}")
            }, {
                showMessage(R.string.notExpectedBehaviour)
                Log.d("My", it.message)
            }
            ))
    }


    private fun insertToDatabase(items: List<CurrentWeatherEntity>) {
        Log.d("My", "Add: ${items.map { it.id }}")
        disposable.add(db.insert(items)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("My", "Add: ${items.map { it.id }}")
                //loadData()
            }, {
                showMessage(R.string.notExpectedBehaviour)
                Log.d("My", it.message)
            }
            ))
    }


    override fun attachView(v: ShowWeatherContract.View) {
        view = v
    }


    override fun detachView() {
        view = null
    }


    override fun showMessage(msg: Int) {
        view?.showMessage(msg)
    }
}