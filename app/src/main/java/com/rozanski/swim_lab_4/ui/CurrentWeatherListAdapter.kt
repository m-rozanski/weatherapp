package com.rozanski.swim_lab_4.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rozanski.swim_lab_4.API_IMG_FORMAT
import com.rozanski.swim_lab_4.API_IMG_URL
import com.rozanski.swim_lab_4.App
import com.rozanski.swim_lab_4.model.currentWeatherResponse.CurrentWeatherResponse
import com.rozanski.swim_lab_4.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recyclerview_current_weather_item.view.*
import kotlin.math.roundToInt


class CurrentWeatherListAdapter(
    var items: ArrayList<CurrentWeatherResponse>, private val listener: MyClickListener
) : RecyclerView.Adapter<CurrentWeatherListAdapter.CurrentWeatherListViewHolder>() {


    interface MyClickListener {
        fun handleItemClicked(currentWeatherResponse: CurrentWeatherResponse)
    }


    inner class CurrentWeatherListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private var cityName: TextView
        private var weatherDescription: TextView
        private var temperature: TextView
        private var logo: ImageView

        init {
            cityName = view.city_name
            weatherDescription = view.weather_description
            logo = view.weather_logo
            temperature = view.temperature
        }


        fun bind(item: CurrentWeatherResponse) {
            cityName.text = item.name
            weatherDescription.text = item.weather[0].description
            val logo_txt = item.weather[0].icon
            Log.d("My", "Loading image: ${API_IMG_URL}${logo_txt}${API_IMG_FORMAT}")
            Picasso.get().load("${API_IMG_URL}${logo_txt}${API_IMG_FORMAT}").into(logo)
            val unit = view.resources.getString(R.string.celsius)
            temperature.text = "${(item.main.temp - 273.15).roundToInt()} $unit"
        }
    }

    override fun getItemCount(): Int = items.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrentWeatherListViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_current_weather_item, parent, false)
        return CurrentWeatherListViewHolder(view)
    }


    override fun onBindViewHolder(holder: CurrentWeatherListViewHolder, position: Int) {
        val current = items[position]
        holder.bind(current)
        holder.itemView.setOnLongClickListener {
            listener.handleItemClicked(current)
            true
        }
    }


    fun delete(item: CurrentWeatherResponse) {
        items.remove(item)
        notifyDataSetChanged()
    }


    fun updateData(data: List<CurrentWeatherResponse>) {
        Log.d("My", "Adapter: update all")
        items = ArrayList(data)
        notifyDataSetChanged()
    }

}