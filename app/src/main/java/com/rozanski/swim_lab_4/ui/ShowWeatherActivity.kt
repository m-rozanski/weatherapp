package com.rozanski.swim_lab_4.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rozanski.swim_lab_4.R
import com.rozanski.swim_lab_4.di.component.DaggerShowWeatherActivityComponent
import com.rozanski.swim_lab_4.di.module.ShowWeatherActivityModule
import com.rozanski.swim_lab_4.model.currentWeatherResponse.CurrentWeatherResponse
import kotlinx.android.synthetic.main.activity_show_weather.*
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class ShowWeatherActivity : AppCompatActivity(), ShowWeatherContract.View, CurrentWeatherListAdapter.MyClickListener {
    @Inject
    lateinit var presenter: ShowWeatherPresenter
    private lateinit var adapter: CurrentWeatherListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_weather)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = getString(R.string.app_name)
        adapter = CurrentWeatherListAdapter(ArrayList(), this)
        injectDependency()
        presenter.attachView(this)
        presenter.setup()
        initView()
        doAsync { presenter.loadData() }
    }

    private fun injectDependency() {
        DaggerShowWeatherActivityComponent.builder()
            .showWeatherActivityModule(ShowWeatherActivityModule(this))
            .build()
            .inject(this)
    }


    private fun initView() {
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter
    }


    override fun updateData(data: List<CurrentWeatherResponse>) {
        adapter.updateData(data)
    }


    override fun showMessage(msg: Int) {
        Toast.makeText(this, getString(msg), Toast.LENGTH_SHORT).show()
    }


    private fun showDialog() {
        val context = this
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.findCity))

        val view = layoutInflater.inflate(R.layout.dialog_text_input_layout, null)
        val categoryEditText = view.findViewById(R.id.input) as EditText

        builder.setView(view)

        builder.setPositiveButton(android.R.string.ok) { dialog, _ ->
            dialog.dismiss()
            presenter.addCity(categoryEditText.text.toString())
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, _ ->
            dialog.cancel()
        }

        builder.show()
    }


    override fun handleItemClicked(currentWeatherResponse: CurrentWeatherResponse) {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.deleteItem)).setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
                adapter.delete(currentWeatherResponse)
                presenter.delete(currentWeatherResponse)
            }.setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }.create().show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_refresh) {
            presenter.refresh()
            return true
        }

        if (id == R.id.action_add_city) {
            showDialog()
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

}