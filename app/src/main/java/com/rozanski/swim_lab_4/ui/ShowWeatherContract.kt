package com.rozanski.swim_lab_4.ui

import com.rozanski.swim_lab_4.model.currentWeatherResponse.CurrentWeatherResponse

interface ShowWeatherContract {

    interface View {
        fun updateData(data: List<CurrentWeatherResponse>)
        fun showMessage(msg: Int)
    }

    interface Presenter {
        fun loadData()
        fun addCity(cityName: String)
        fun refresh()
        fun delete(item: CurrentWeatherResponse)
        fun showMessage(msg: Int)

        fun attachView(v: View)
        fun detachView()
    }
}