package com.rozanski.swim_lab_4.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rozanski.swim_lab_4.DATABASE_CURRENT_WEATHER_TABLE_NAME

@Entity(tableName = DATABASE_CURRENT_WEATHER_TABLE_NAME)
data class CurrentWeatherEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "data") val data: String
)
