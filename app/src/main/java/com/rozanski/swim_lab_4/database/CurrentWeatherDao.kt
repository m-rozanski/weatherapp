package com.rozanski.swim_lab_4.database

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface CurrentWeatherDao {
    @Query("SELECT * FROM currentWeather")
    fun getAll(): Flowable<List<CurrentWeatherEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currentWeatherEntity: CurrentWeatherEntity): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currentWeatherEntityList: List<CurrentWeatherEntity>): Completable

    @Delete
    fun delete(currentWeatherEntity: CurrentWeatherEntity): Completable
}