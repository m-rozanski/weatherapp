package com.rozanski.swim_lab_4.database

import android.util.Log
import com.rozanski.swim_lab_4.di.component.DaggerCurrentWeatherRepositoryComponent
import com.rozanski.swim_lab_4.di.module.DatabaseModule
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class CurrentWeatherRepository {
    private var currentWeatherList: MutableList<CurrentWeatherEntity> = mutableListOf()
    @Inject
    lateinit var database: CurrentWeatherDatabase
    private lateinit var dao: CurrentWeatherDao

    fun setup() {
        DaggerCurrentWeatherRepositoryComponent
            .builder()
            .databaseModule(DatabaseModule)
            .build()
            .inject(this)
        dao = database.currentWeatherDao()
    }

    fun getAll(): Flowable<List<CurrentWeatherEntity>> {
        Log.d("My", "Database: getAll")
        return dao.getAll()
    }

    fun insert(currentWeatherEntity: CurrentWeatherEntity): Completable {
        Log.d("My", "Database: insert")
        return dao.insert(currentWeatherEntity)
    }

    fun insert(currentWeatherEntityList: List<CurrentWeatherEntity>): Completable {
        Log.d("My", "Database: insert list")
        currentWeatherList = currentWeatherEntityList.toMutableList()
        return dao.insert(currentWeatherEntityList)
    }

    fun delete(currentWeatherEntity: CurrentWeatherEntity): Completable {
        Log.d("My", "Database: delete")
        return dao.delete(currentWeatherEntity)
    }

}