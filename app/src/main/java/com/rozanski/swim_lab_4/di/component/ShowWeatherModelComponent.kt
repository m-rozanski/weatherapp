package com.rozanski.swim_lab_4.di.component

import com.rozanski.swim_lab_4.model.ShowWeatherModel
import com.rozanski.swim_lab_4.di.module.ApiModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class])
interface ShowWeatherModelComponent {
    fun inject(showWeatherModel: ShowWeatherModel)

    @Component.Builder
    interface Builder {
        fun build(): ShowWeatherModelComponent
        fun apiModule(apiModule: ApiModule): Builder
    }
}