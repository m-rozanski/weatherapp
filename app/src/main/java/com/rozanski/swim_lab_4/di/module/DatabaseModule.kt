package com.rozanski.swim_lab_4.di.module

import androidx.room.Room
import com.rozanski.swim_lab_4.App
import com.rozanski.swim_lab_4.DATABASE_NAME
import com.rozanski.swim_lab_4.database.CurrentWeatherDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(): CurrentWeatherDatabase {
        return Room.databaseBuilder(App.instance, CurrentWeatherDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }
}