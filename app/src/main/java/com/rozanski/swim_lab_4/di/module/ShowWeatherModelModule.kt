package com.rozanski.swim_lab_4.di.module

import com.rozanski.swim_lab_4.model.ShowWeatherModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ShowWeatherModelModule {

    @Provides
    @Singleton
    fun provideShowWeatherModel(): ShowWeatherModel {
        return ShowWeatherModel()
    }
}