package com.rozanski.swim_lab_4.di.module

import android.app.Activity
import com.rozanski.swim_lab_4.ui.ShowWeatherPresenter
import dagger.Module
import dagger.Provides

@Module
class ShowWeatherActivityModule(private var activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun providePresenter(): ShowWeatherPresenter {
        return ShowWeatherPresenter()
    }

}