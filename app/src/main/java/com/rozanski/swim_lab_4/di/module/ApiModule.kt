package com.rozanski.swim_lab_4.di.module

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.rozanski.swim_lab_4.API_BASE_URL
import com.rozanski.swim_lab_4.network.ApiWeatherInterface
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory

@Module
object ApiModule {

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideApi(): ApiWeatherInterface {
        val client = OkHttpClient.Builder()
            .addNetworkInterceptor(StethoInterceptor()).build()

        val retrofit = retrofit2.Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .baseUrl(API_BASE_URL)
            .build()

        return retrofit.create(ApiWeatherInterface::class.java)
    }
}