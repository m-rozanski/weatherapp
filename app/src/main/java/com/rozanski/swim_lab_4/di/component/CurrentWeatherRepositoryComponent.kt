package com.rozanski.swim_lab_4.di.component

import com.rozanski.swim_lab_4.database.CurrentWeatherRepository
import com.rozanski.swim_lab_4.di.module.DatabaseModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DatabaseModule::class])
interface CurrentWeatherRepositoryComponent {
    fun inject(currentWeatherRepository: CurrentWeatherRepository)

    @Component.Builder
    interface Builder {
        fun build(): CurrentWeatherRepositoryComponent
        fun databaseModule(databaseModule: DatabaseModule): Builder
    }
}