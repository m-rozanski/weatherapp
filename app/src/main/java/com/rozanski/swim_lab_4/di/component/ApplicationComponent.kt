package com.rozanski.swim_lab_4.di.component

import com.rozanski.swim_lab_4.App
import com.rozanski.swim_lab_4.di.module.ApplicationModule
import dagger.Component

@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(application: App)

}