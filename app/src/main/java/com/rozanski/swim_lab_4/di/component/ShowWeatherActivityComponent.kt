package com.rozanski.swim_lab_4.di.component

import com.rozanski.swim_lab_4.di.module.ShowWeatherActivityModule
import com.rozanski.swim_lab_4.ui.ShowWeatherActivity
import dagger.Component

@Component(modules = [ShowWeatherActivityModule::class])
interface ShowWeatherActivityComponent {

    fun inject(showWeatherActivity: ShowWeatherActivity)

}