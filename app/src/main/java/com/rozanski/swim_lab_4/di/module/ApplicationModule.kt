package com.rozanski.swim_lab_4.di.module

import android.app.Application
import com.rozanski.swim_lab_4.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return app
    }
}