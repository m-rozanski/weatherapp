package com.rozanski.swim_lab_4.di.component

import com.rozanski.swim_lab_4.di.module.ShowWeatherModelModule
import com.rozanski.swim_lab_4.ui.ShowWeatherPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ShowWeatherModelModule::class])
interface ShowWeatherPresenterComponent {
    fun inject(showWeatherPresenter: ShowWeatherPresenter)

    @Component.Builder
    interface Builder {
        fun build(): ShowWeatherPresenterComponent
        fun showWeatherModelModule(showWeatherModelModule: ShowWeatherModelModule): Builder
    }
}