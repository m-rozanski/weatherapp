package com.rozanski.swim_lab_4

import com.google.gson.Gson
import com.rozanski.swim_lab_4.model.currentWeatherResponse.CurrentWeatherResponse
import com.rozanski.swim_lab_4.database.CurrentWeatherEntity

const val DATABASE_CURRENT_WEATHER_TABLE_NAME = "currentWeather"
const val DATABASE_NAME = "currentWeather_database"

const val API_BASE_URL = "http://api.openweathermap.org/data/2.5/"
const val API_IMG_URL = "http://openweathermap.org/img/w/"
const val API_IMG_FORMAT = ".png"
const val API_KEY = "18583445131a2c727506dd012e8c2455"


private val gson = Gson()

fun convertToEntity(currentWeatherResponse: CurrentWeatherResponse): CurrentWeatherEntity =
    CurrentWeatherEntity(currentWeatherResponse.id, gson.toJson(currentWeatherResponse))


fun convertToResponse(entity: CurrentWeatherEntity): CurrentWeatherResponse =
    gson.fromJson(entity.data, CurrentWeatherResponse::class.java)