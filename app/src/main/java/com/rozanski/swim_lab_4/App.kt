package com.rozanski.swim_lab_4

import android.app.Application
import com.facebook.stetho.Stetho
import com.rozanski.swim_lab_4.di.component.ApplicationComponent
import com.rozanski.swim_lab_4.di.component.DaggerApplicationComponent
import com.rozanski.swim_lab_4.di.module.ApplicationModule

class App : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        //prefs = Prefs(applicationContext)
        //Log.d("My", "prefs created")
        super.onCreate()

        Stetho.initializeWithDefaults(this)

        instance = this
        setup()
    }

    private fun setup() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance: App private set
        //var prefs: Prefs? = null
    }
}