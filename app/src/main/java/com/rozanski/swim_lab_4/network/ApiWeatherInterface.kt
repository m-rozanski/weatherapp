package com.rozanski.swim_lab_4.network

import com.rozanski.swim_lab_4.API_KEY
import com.rozanski.swim_lab_4.model.currentWeatherResponse.CurrentWeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiWeatherInterface {
    @GET("weather?mode=json&APPID=${API_KEY}")
    fun getCurrentWeather(@Query("q") city: String): Call<CurrentWeatherResponse>
}