package com.rozanski.swim_lab_4.model

import android.util.Log
import com.rozanski.swim_lab_4.R
import com.rozanski.swim_lab_4.di.component.DaggerShowWeatherModelComponent
import com.rozanski.swim_lab_4.di.module.ApiModule
import com.rozanski.swim_lab_4.model.currentWeatherResponse.CurrentWeatherResponse
import com.rozanski.swim_lab_4.network.ApiWeatherInterface
import com.rozanski.swim_lab_4.ui.ShowWeatherPresenter
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class ShowWeatherModel {
    @Inject
    lateinit var api: ApiWeatherInterface

    init {
        DaggerShowWeatherModelComponent
            .builder()
            .apiModule(ApiModule)
            .build()
            .inject(this)
    }

    fun getCurrentWeather(presenter: ShowWeatherPresenter, cityName: String, code: Int) {
        val call = api.getCurrentWeather(cityName)
        Log.d("My", "API: $cityName")
        call.enqueue(object : retrofit2.Callback<CurrentWeatherResponse> {
            override fun onResponse(call: Call<CurrentWeatherResponse>, response: Response<CurrentWeatherResponse>) {

                when (response.code()) {
                    200 -> {
                        val currentWeather = response.body()
                        presenter.onApiResponse(currentWeather, code)
                    }
                    404 -> presenter.showMessage(R.string.noCityFound)
                    else -> {
                        presenter.showMessage(R.string.apiFailed)
                        Log.d("My", "Failed with code ${response.code()}")
                    }
                }
            }

            override fun onFailure(call: Call<CurrentWeatherResponse>, t: Throwable) {
                Log.d("My", t.message)
                presenter.showMessage(R.string.apiFailed)
            }
        })
    }
}