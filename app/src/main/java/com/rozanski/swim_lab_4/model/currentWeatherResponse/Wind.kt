package com.rozanski.swim_lab_4.model.currentWeatherResponse

data class Wind(
    val deg: Double,
    val speed: Double
)