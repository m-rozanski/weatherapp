package com.rozanski.swim_lab_4.model.currentWeatherResponse

data class CurrentWeatherResponse(
    val base: String,
    val clouds: Clouds,
    val cod: Int,
    val coord: Coordinates,
    val dt: Int,
    val id: Int,
    val main: WeatherValues,
    val name: String,
    val sys: Sys,
    val visibility: Int,
    val weather: List<WeatherConditionDetails>,
    val wind: Wind
)