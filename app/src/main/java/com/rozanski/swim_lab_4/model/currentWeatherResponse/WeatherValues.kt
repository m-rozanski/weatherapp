package com.rozanski.swim_lab_4.model.currentWeatherResponse

data class WeatherValues(
    val humidity: Double,
    val pressure: Double,
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double
)