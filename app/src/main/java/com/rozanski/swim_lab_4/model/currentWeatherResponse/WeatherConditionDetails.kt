package com.rozanski.swim_lab_4.model.currentWeatherResponse

data class WeatherConditionDetails(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)