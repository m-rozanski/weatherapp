package com.rozanski.swim_lab_4.model.currentWeatherResponse

data class Coordinates(
    val lat: Double,
    val lon: Double
)