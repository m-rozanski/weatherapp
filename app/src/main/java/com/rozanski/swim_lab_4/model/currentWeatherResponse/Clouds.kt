package com.rozanski.swim_lab_4.model.currentWeatherResponse

data class Clouds(
    val all: Int
)